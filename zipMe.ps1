﻿$currentDir = $MyInvocation.MyCommand.Path | Split-Path -Parent
$include  = @("io", "models","solver")
# get files to compress using exclusion filer
$files = Get-ChildItem -Path $currentDir -Include $include -Recurse

Write-Host $currentDir
Write-Host $files
Compress-Archive -Force -Path $files -DestinationPath archive.zip
