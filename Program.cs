﻿using System.Diagnostics;
using hashcode_2022.io;
using hashcode_2022.solver;

var totalTimer = new Stopwatch();
totalTimer.Start();

var tasks = new List<Task<long>>();

tasks.Add(RunWithFile(FileName.A));
tasks.Add(RunWithFile(FileName.B));
tasks.Add(RunWithFile(FileName.C));
tasks.Add(RunWithFile(FileName.D));
tasks.Add(RunWithFile(FileName.E));
tasks.Add(RunWithFile(FileName.F));

await Task.WhenAll(tasks);

totalTimer.Stop();

Console.WriteLine("Total Score: " + tasks.Select(t => t.Result).Sum());
Console.WriteLine("Total Time: " + totalTimer.Elapsed.Milliseconds + "ms");
Console.WriteLine("Total Time: " + totalTimer.Elapsed.TotalSeconds + "s");


Console.WriteLine("Press any key to continue...");
// Console.ReadKey();

Task<long> RunWithFile(string fileName)
{
    CancellationTokenSource s_cts = new CancellationTokenSource();
    s_cts.CancelAfter(TimeSpan.FromMinutes(2));
    return Task.Run(async () =>
    {
        var displayName = fileName.PadRight(20);
        // Console.WriteLine($"[{displayName}] Starting set");
        var watch = new Stopwatch();
        watch.Start();
        var lines = Reader.ReaderIn(fileName);
        // run Solver class with the lines
        var solver = new Solver(displayName, lines);
        Writer.WriteLines(fileName, solver.Output());
        var score = await solver.Score;
        watch.Stop();
        Console.WriteLine($"[{displayName}] Score {fileName}: {score} @ {watch.Elapsed.TotalSeconds}s");
        // Console.WriteLine($"[{displayName}] {watch.ElapsedMilliseconds} ms");
        s_cts.Dispose();
        return score;
    }, s_cts.Token);
}