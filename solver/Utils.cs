using hashcode_2022.models;

namespace hashcode_2022.solver;

public static class Utils
{
    public static long Score(IEnumerable<string> enumerable)
    {
        return 0;
    }

    public static void UpdateSkills(AssignedProject assignment)
    {
        foreach (var (role, boi) in assignment.Contributors)
        {
            if (role.SkillLevel >= boi.GetSkill(role.SkillName).Level)
            {
                boi.BoostSkill(role.SkillName);
            }
        }
    }
}