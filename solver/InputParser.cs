using hashcode_2022.models;
using static System.Int32;

namespace hashcode_2022.solver;

public static class InputParser
{
    private const string Space = " ";

    public class Input
    {
        public List<Contributor> Contributors;
        public List<Project> Projects;
        public List<string> Skills;

        public Input(List<Contributor> contributors, List<Project> projects, List<string> skills)
        {
            Contributors = contributors;
            Projects = projects;
            Skills = skills;
        }
    }

    public static Input ParseInput(List<string> input)
    {
        List<Contributor> ContributorList = new();
        List<Project> ProjectList = new();

        var inputIndex = 0;
        var inputs = input[inputIndex++].Split(Space);
        var contributors = Parse(inputs[0]);
        var projects = Parse(inputs[1]);
        while (contributors > 0)
        {
            var strings = input[inputIndex++].Split(Space);
            var name = strings[0];
            var skills = Parse(strings[1]);
            var contributor = new Contributor(name);
            ContributorList.Add(contributor);
            while (skills > 0)
            {
                var skillParams = input[inputIndex++].Split(Space);
                var skillName = skillParams[0];
                var level = Parse(skillParams[1]);
                contributor.AddSkill(skillName, level);
                skills--;
            }

            contributors--;
        }

        while (projects > 0)
        {
            var projectParams = input[inputIndex++].Split(Space);
            var name = projectParams[0];
            var duration = Parse(projectParams[1]);
            var score = Parse(projectParams[2]);
            var lastDate = Parse(projectParams[3]);
            var roles = Parse(projectParams[4]);

            var project = new Project(name, duration, score, lastDate);
            ProjectList.Add(project);
            for (int i = 0; i < roles; i++)
            {
                var roleParams = input[inputIndex++].Split(Space);
                var skillName = roleParams[0];
                var level = Parse(roleParams[1]);
                project.AddRole(i, skillName, level);
            }

            projects--;
        }

        var SkillList = ContributorList.SelectMany(c => c.Skills).Select(skill => skill.Name).Distinct().ToList();

        foreach (var contributor in ContributorList)
        {
            var noSkills = SkillList.Where(s => !contributor.Skills.Select(skill => skill.Name).Any(s1 => s1 == s));
            foreach (var skill in noSkills)
            {
                contributor.AddSkill(skill);
            }
        }

        return new Input(ContributorList, ProjectList, SkillList);
    }
}