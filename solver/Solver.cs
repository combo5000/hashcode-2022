﻿using System.Linq;
using hashcode_2022.models;

namespace hashcode_2022.solver;

public class Solver
{
    public Task<long> Score { get; private set; }

    private readonly string _displayName;
    private readonly IEnumerable<string> _output;

    public Solver(string fileName, List<string> input)
    {
        _displayName = fileName.PadRight(12);
        var data = InputParser.ParseInput(input);
        _output = Solve(data);
        Score = Task.Run(ScoreResult);
    }

    private long ScoreResult()
    {
        return Utils.Score(_output);
    }

    private IEnumerable<string> Solve(InputParser.Input input)
    {
        var assignable = input.Projects
            .OrderBy(project => project.LastDate1)
            .ThenBy(project => project.Duration1)
            .ToList();

        var unasignable = new List<Project>();

        var furthestDate = input.Projects.MaxBy(project => project.LastUsefulDate)!.LastUsefulDate;

        var assignedProjects = new List<AssignedProject>();
        var workSchedule = new Dictionary<string, int>();

        var currentTick = 0;

        while (currentTick < furthestDate && assignable.Any())
        {
            while (assignable.Any())
            {
                var planProject = assignable.First();
                if (workSchedule.Keys.Count >= input.Contributors.Count)
                {
                    var min = workSchedule.Values.Min();
                    workSchedule = workSchedule.Where((k, v) => v > min).ToDictionary(pair => pair.Key, pair => pair.Value);
                    currentTick = min;
                }

                var assignment = new AssignedProject(planProject)
                {
                    StartTick = currentTick
                };

                Dictionary<Role, Contributor> team;
                try
                {
                    var availableContribs = input.Contributors.Where(c => !workSchedule.ContainsKey(c.Name)).ToList();
                    team = FindTeam(assignment, availableContribs);

                    assignment.AssignTeam(team);
                    var endDate = currentTick + planProject.Duration1;
                    UpdateWorkSchedule(team.Values, workSchedule, endDate);
                    Utils.UpdateSkills(assignment);
                    assignedProjects.Add(assignment);
                }
                catch (Exception e)
                {
                    unasignable.Add(planProject);
                }
                finally
                {
                    assignable.Remove(planProject);
                }
            }
            
            workSchedule = new Dictionary<string, int>();
            while (unasignable.Any())
            {
                var planProject = unasignable.First();
                if (workSchedule.Keys.Count >= input.Contributors.Count)
                {
                    var min = workSchedule.Values.Min();
                    workSchedule = workSchedule.Where((k, v) => v > min).ToDictionary(pair => pair.Key, pair => pair.Value);
                    currentTick = min;
                }

                var assignment = new AssignedProject(planProject)
                {
                    StartTick = currentTick
                };

                Dictionary<Role, Contributor> team;
                try
                {
                    var availableContribs = input.Contributors.Where(c => !workSchedule.ContainsKey(c.Name)).ToList();
                    team = FindTeam(assignment, availableContribs);

                    assignment.AssignTeam(team);
                    var endDate = currentTick + planProject.Duration1;
                    UpdateWorkSchedule(team.Values, workSchedule, endDate);
                    Utils.UpdateSkills(assignment);
                    assignedProjects.Add(assignment);
                }
                catch (Exception e)
                {
                    // unasignable.Add(planProject);
                }
                finally
                {
                    unasignable.Remove(planProject);
                }
            }
        }

        return Stringify(assignedProjects);
    }

    private static void UpdateWorkSchedule(IEnumerable<Contributor> team, Dictionary<string, int> workSchedule,
        int currentTick)
    {
        foreach (var suicidalManiac in team)
        {
            var suicidalManiacName = suicidalManiac.Name;
            if (workSchedule.ContainsKey(suicidalManiacName))
            {
                workSchedule.Remove(suicidalManiacName);
            }

            workSchedule.Add(suicidalManiac.Name, currentTick);
        }
    }


    private IEnumerable<string> Stringify(List<AssignedProject> assignedProjects)
    {
        var output = new List<string>();
        output.Add($"{assignedProjects.Count}");

        foreach (var assignedProject in assignedProjects)
        {
            output.Add($"{assignedProject.Project.Name}");
            var victims = String.Join(" ", assignedProject.Contributors
                .OrderBy(pair => pair.Key.Index)
                .Select(c => c.Value.Name)
                .ToList());
            output.Add($"{victims}");
        }

        return output;
    }

    private Dictionary<Role, Contributor> FindTeam(AssignedProject project, List<Contributor> contributors)
    {
        var assignments = new Dictionary<Role, Contributor>();
        var people = new List<Contributor>();

        foreach (var role in project.Project.Roles1)
        {
            var mentors = people.Where(contributor => contributor.Skills.Any(skill => skill.Name == role.SkillName && skill.Level >= role.SkillLevel));

            var eligible = contributors
                .Where(contributor => !people.Contains(contributor))
                .Where(contributor =>
                {
                    if (mentors.Any())
                    {
                        var able = contributor.Skills.Any(skill =>
                            skill.Name == role.SkillName && skill.Level >= role.SkillLevel - 1);
                        if (able && contributor.GetSkill(role.SkillName).Level < role.SkillLevel)
                        {
                            Log($"Assigning {contributor.Name} to {role.SkillName} with supervision by {mentors.First().Name}");
                        }
                        return able;
                    }

                    return contributor.Skills.Any(skill =>
                        skill.Name == role.SkillName && skill.Level >= role.SkillLevel);
                });

            var patsy = eligible.First();

            people.Add(patsy);
            assignments.Add(role, patsy);
        }

        return assignments;
    }

    public List<string> Output()
    {
        // Log($"Output: {_output.Count()} {string.Join(' ', _output)}");
        return _output.ToList();
    }

    public void Log(string s)
    {
        Console.WriteLine($"[{_displayName}] {s}");
    }
}