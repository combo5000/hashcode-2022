﻿namespace hashcode_2022.io;

public static class FileName
{
    public const string A = "a_an_example";
    public const string B = "b_better_start_small";
    public const string C = "c_collaboration";
    public const string D = "d_dense_schedule";
    public const string E = "e_exceptional_skills";
    public const string F = "f_find_great_mentors";
}