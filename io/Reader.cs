﻿namespace hashcode_2022.io
{
    internal static class Reader
    {
        public static List<string> ReaderIn(string fileName)
        {
            var currentDirectory = Environment.CurrentDirectory.Split("bin")[0];
            var path = Path.Combine(currentDirectory, @"files\in\", fileName + ".in");
            var lines = new List<string>();
            try
            {
                // Open the text file using a stream reader.
                using var sr = new StreamReader(path);
                // Read the stream to a string, and write the string to the console.
                while (!sr.EndOfStream)
                {
                    var s = sr.ReadLine();
                    lines.Add(s!);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                throw new Exception("something went wrong so we crash the place");
            }

            return lines;
        }
    }
}