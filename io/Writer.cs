﻿namespace hashcode_2022.io
{
    internal static class Writer
    {
        public static void WriteLines(string fileName, List<string> lines)
        {
            var currentDirectory = Environment.CurrentDirectory.Split("bin")[0];
            var directory = Path.Combine(currentDirectory, @"files\out\");
            var path = Path.Combine(directory, fileName + ".out");
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                Directory.CreateDirectory(directory);

                using var file = new StreamWriter(path);
                foreach (var line in lines)
                {
                    file.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be written:");
                Console.WriteLine(e.Message);
                throw new Exception("something went wrong so we crash the place");
            }
        }
    }
}