﻿using System.Diagnostics;

namespace hashcode_2022.models;

public class Project
{
    private string _name;
    private int Duration;
    private int LastDate;
    private int Score;
    private List<Role> Roles;

    public int LastUsefulDate => LastDate + Duration;

    public Project(string name, int duration, int score, int lastDate)
    {
        _name = name;
        Duration = duration;
        LastDate = lastDate;
        Score = score;
        Roles = new List<Role>();
    }

    public void AddRole(int index, string skillName, int skillLevel)
    {
        Roles.Add(new Role(index, skillName, skillLevel));
        Roles = Roles.OrderBy(role => role.SkillLevel).ToList();
    }

    public string Name
    {
        get => _name;
        set => _name = value ?? throw new ArgumentNullException(nameof(value));
    }

    public int Duration1
    {
        get => Duration;
        set => Duration = value;
    }

    public int LastDate1
    {
        get => LastDate;
        set => LastDate = value;
    }

    public int Score1
    {
        get => Score;
        set => Score = value;
    }

    public List<Role> Roles1
    {
        get => Roles;
        set => Roles = value ?? throw new ArgumentNullException(nameof(value));
    }
}

[DebuggerDisplay("{SkillName}@{SkillLevel} i:{Index}")]
public class Role
{
    private int index;
    private string skillName;
    private int skillLevel;

    public Role(int index, string skillName, int skillLevel)
    {
        this.index = index;
        this.skillName = skillName;
        this.skillLevel = skillLevel;
    }

    public int Index
    {
        get => index;
        set => index = value;
    }

    public string SkillName
    {
        get => skillName;
        set => skillName = value ?? throw new ArgumentNullException(nameof(value));
    }

    public int SkillLevel
    {
        get => skillLevel;
        set => skillLevel = value;
    }
}