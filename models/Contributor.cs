﻿using System.Diagnostics;

namespace hashcode_2022.models;

[DebuggerDisplay("Contributor:{Name}")]
public class Contributor
{
    private string _name;
    private List<Skill> _skills;
    private Dictionary<string, Skill> _skillsDict;
    private Dictionary<string, Skill> _baseSkillsDict;

    public string Name
    {
        get => _name;
        set => _name = value ?? throw new ArgumentNullException(nameof(value));
    }

    public List<Skill> Skills
    {
        get => _skills;
        set => _skills = value ?? throw new ArgumentNullException(nameof(value));
    }

    public Contributor(string name)
    {
        _name = name;
        _skills = new List<Skill>();
        _skillsDict = new Dictionary<string, Skill>();
        _baseSkillsDict = new Dictionary<string, Skill>();
    }

    public void AddSkill(string name, int level = 0)
    {
        var skill = new Skill(level, name);
        _skills.Add(skill);
        _skillsDict.Add(name, skill);
        _baseSkillsDict.Add(name, new Skill(level, name));
    }

    public void BoostSkill(string name)
    {
        _skillsDict[name].Level++;
    }

    public Skill GetSkill(string name)
    {
        return _skillsDict[name];
    }
}


[DebuggerDisplay("{Name}@{Level}")]
public class Skill
{
    private long level;
    private string name;

    public Skill(long level, string name)
    {
        this.level = level;
        this.name = name;
    }

    public long Level
    {
        get => level;
        set => level = value;
    }

    public string Name
    {
        get => name;
        set => name = value ?? throw new ArgumentNullException(nameof(value));
    }
}