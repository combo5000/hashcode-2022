using System.Threading.Tasks.Sources;

namespace hashcode_2022.models;

public class AssignedProject
{
    public AssignedProject(Project project)
    {
        Project = project;
        Contributors = new Dictionary<Role, Contributor>();
    }

    public Project Project { get; }
    public int StartTick { get; set; }
    public long Score => CalcScore();
    public Dictionary<Role, Contributor> Contributors { get; private set; }

    private long CalcScore()
    {
        var totalOverTime = StartTick + Project.Duration1 - Project.LastDate1;
        if (totalOverTime > 0)
        {
            var projectScore = Project.Score1 - totalOverTime;
            return projectScore < 0 ? 0 : projectScore;
        }

        return Project.Score1;
    }

    public void AssignTeam(Dictionary<Role, Contributor> team)
    {
        Contributors = team;
    }
}